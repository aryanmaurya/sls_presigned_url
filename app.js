const aws = require("aws-sdk");
const express = require("express");
const { v4: uuidv4 } = require("uuid");
const app = express();
app.use(express.json());
app.use((req, res, next) => {
  req.reqId = uuidv4();
  next();
});

const s3 = new aws.S3({ region: "ap-south-1" });

app.get("/getUrl", async (req, res) => {
  try {
    const url = await s3.getSignedUrlPromise("putObject", {
      Bucket: req.body.BUCKET,
      Key: req.body.FILE_NAME,
      Expires: req.body.EXPIRES_IN / 1000 || 60 * 2,
    });
    console.log({ reqId: req.reqId });
    res.status(200).json({
      success: true,
      data: { uuid: req.reqId, url: url },
      error: null,
      message: "PresignedURL generated successfully.",
    });
  } catch (err) {
    console.log({ reqId: req.reqId, error: err.message });
    res.status(500).json({
      success: false,
      data: null,
      error: err,
      message: `Some internal error while generating presigned URL with reqId: ${req.reqId}`,
    });
  }
});

module.exports = app;
